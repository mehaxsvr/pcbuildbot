using System.Collections.Generic;
using System.Threading.Tasks;
using PCBuild.RemoteApi.Interface.Models;
using RestSharp;

namespace PCBuild.RemoteApi.Interface.Repositories
{
    public class RemoteApiRepository<TModel>
        : IApiRepository<TModel>
        where TModel : ApiModel
    {
        private const string BaseUrl = "http://api.pcbuild.mehax.info";
        
        private string _endpoint;
        private string[] _includes;
        
        private RestClient _client;

        public RemoteApiRepository(string endpoint, string[] includes = null)
        {
            _endpoint = endpoint;
            _includes = includes;

            _client = new RestClient(BaseUrl);
        }

        public async Task<List<TModel>> GetAll()
        {
            return await ExecuteRequest<List<TModel>>();
        }

        public async Task<TModel> GetById(int id)
        {
            return await ExecuteRequest<TModel>(id);
        }

        public async Task<TModel> Post(TModel body)
        {
            return await ExecuteRequest<TModel>(null, Method.POST, body);
        }

        public async Task<TModel> Put(int id, TModel body)
        {
            return await ExecuteRequest<TModel>(id, Method.PUT, body);
        }

        public async Task<bool> Delete(int id)
        {
            return await ExecuteRequest<bool>(id, Method.DELETE);
        }
        
        private async Task<TResponse> ExecuteRequest<TResponse>(int? id = null,
            Method method = Method.GET,
            TModel requestBody = null)
        {
            var endpoint = id != null ? $"{_endpoint}/{id}" : $"{_endpoint}";
            
            var request = new RestRequest(endpoint, method);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            
            if (_includes != null) foreach (var include in _includes)
            {
                request.AddParameter("includes[]", include);
            }
            
            if (requestBody != null)
            {
                request.AddJsonBody(requestBody);
            }

            var response = await _client.ExecuteTaskAsync<ResponseModel<TResponse>>(request);
            return response.IsSuccessful ? response.Data.Data : default;
        }
    }
}