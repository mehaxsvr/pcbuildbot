﻿using PCBuild.RemoteApi.Interface.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PCBuild.RemoteApi.Interface.Repositories
{
    public interface IApiRepository<TModel>
        where TModel : ApiModel
    {
        Task<List<TModel>> GetAll();
        Task<TModel> GetById(int id);
        Task<TModel> Post(TModel body);
        Task<TModel> Put(int id, TModel body);
        Task<bool> Delete(int id);
    }
}
