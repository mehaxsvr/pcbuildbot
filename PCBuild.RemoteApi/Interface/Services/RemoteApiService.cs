﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PCBuild.RemoteApi.Interface.Models;
using PCBuild.RemoteApi.Interface.Repositories;

namespace PCBuild.RemoteApi.Interface.Services
{
    public class RemoteApiService<TModel> : IApiService<TModel>
        where TModel : ApiModel
    {
        private IApiRepository<TModel> _repository;

        public RemoteApiService(IApiRepository<TModel> repository)
        {
            _repository = repository;
        }

        public async Task<List<TModel>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<TModel> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<TModel> Create(TModel model)
        {
            return await _repository.Post(model);
        }

        public async Task<TModel> UpdateById(int id, TModel model)
        {
            return await _repository.Put(id, model);
        }

        public async Task<bool> DeleteById(int id)
        {
            return await _repository.Delete(id);
        }
    }
}
