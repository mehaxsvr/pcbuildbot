﻿using PCBuild.RemoteApi.Interface.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PCBuild.RemoteApi.Interface.Services
{
    public interface IApiService<TModel>
        where TModel : ApiModel
    {
        Task<List<TModel>> GetAll();
        Task<TModel> GetById(int id);
        Task<TModel> Create(TModel model);
        Task<TModel> UpdateById(int id, TModel model);
        Task<bool> DeleteById(int id);
    }
}
