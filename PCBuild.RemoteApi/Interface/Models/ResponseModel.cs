
namespace PCBuild.RemoteApi.Interface.Models
{
    public sealed class ResponseModel<TModel>
    {
        public bool? Status { get; set; }
        public string Message { get; set; }
        public TModel Data { get; set; }
    }
}
