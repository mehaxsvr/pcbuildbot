using System;

namespace PCBuild.RemoteApi.Interface.Models
{
    public partial class ApiModel
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}