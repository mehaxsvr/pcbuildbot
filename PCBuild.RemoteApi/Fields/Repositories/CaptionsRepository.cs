using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Repositories;

namespace PCBuild.RemoteApi.Fields.Repositories
{
    public class CaptionsRepository : RemoteApiRepository<CaptionModel>
    {
        public CaptionsRepository() : base("captions", new [] {"parent", "type", "field"})
        {
        }
    }
}