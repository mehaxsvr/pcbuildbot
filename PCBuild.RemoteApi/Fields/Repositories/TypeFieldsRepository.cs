using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Repositories;

namespace PCBuild.RemoteApi.Fields.Repositories
{
    public class TypeFieldsRepository : RemoteApiRepository<TypeFieldModel>
    {
        public TypeFieldsRepository() : base("type-fields", new[] {"type", "field"})
        {
        }
    }
}
