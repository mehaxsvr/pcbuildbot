using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Repositories;

namespace PCBuild.RemoteApi.Fields.Repositories
{
    public class FieldsRepository : RemoteApiRepository<FieldModel>
    {
        public FieldsRepository() : base("fields")
        {
        }
    }
}