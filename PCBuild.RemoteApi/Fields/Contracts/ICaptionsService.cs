using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuild.RemoteApi.Fields.Contracts
{
    public interface ICaptionsService : IApiService<CaptionModel>
    {
    }
}