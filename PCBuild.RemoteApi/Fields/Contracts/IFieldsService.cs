using System.Collections.Generic;
using System.Threading.Tasks;
using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuild.RemoteApi.Fields.Contracts
{
    public interface IFieldsService : IApiService<FieldModel>
    {
        Task<List<TypeFieldModel>> GetAllTypeFields();
        Task<TypeFieldModel> GetAllTypeFields(int id);
        Task<TypeFieldModel> CreateTypeField(TypeFieldModel model);
        Task<TypeFieldModel> UpdateTypeFieldById(int id, TypeFieldModel model);
        Task<bool> DeleteTypeFieldById(int id);
    }
}