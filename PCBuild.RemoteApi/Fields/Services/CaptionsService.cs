using PCBuild.RemoteApi.Fields.Contracts;
using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Repositories;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuild.RemoteApi.Fields.Services
{
    public class CaptionsService : RemoteApiService<CaptionModel>, ICaptionsService
    {
        public CaptionsService(IApiRepository<CaptionModel> repository) : base(repository)
        {
        }
    }
}