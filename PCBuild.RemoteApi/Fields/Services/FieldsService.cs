using System.Collections.Generic;
using System.Threading.Tasks;
using PCBuild.RemoteApi.Fields.Contracts;
using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Interface.Repositories;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuild.RemoteApi.Fields.Services
{
    public class FieldsService : RemoteApiService<FieldModel>, IFieldsService
    {
        private IApiRepository<TypeFieldModel> _fieldCaptionsRepository;

        public FieldsService(
            IApiRepository<FieldModel> repository,
            IApiRepository<TypeFieldModel> fieldCaptionsRepository
        ) : base(repository)
        {
            _fieldCaptionsRepository = fieldCaptionsRepository;
        }

        public async Task<List<TypeFieldModel>> GetAllTypeFields()
        {
            return await _fieldCaptionsRepository.GetAll();
        }

        public async Task<TypeFieldModel> GetAllTypeFields(int id)
        {
            return await _fieldCaptionsRepository.GetById(id);
        }

        public async Task<TypeFieldModel> CreateTypeField(TypeFieldModel model)
        {
            return await _fieldCaptionsRepository.Post(model);
        }

        public async Task<TypeFieldModel> UpdateTypeFieldById(int id, TypeFieldModel model)
        {
            return await _fieldCaptionsRepository.Put(id, model);
        }

        public async Task<bool> DeleteTypeFieldById(int id)
        {
            return await _fieldCaptionsRepository.Delete(id);
        }
    }
}