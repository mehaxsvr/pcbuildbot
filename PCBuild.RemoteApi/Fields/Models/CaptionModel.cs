using PCBuild.RemoteApi.Interface.Models;

namespace PCBuild.RemoteApi.Fields.Models
{
    public class CaptionModel : ApiModel
    {
        public bool IsMain { get; set; }
        public int? ParentId { get; set; }
        public int? TypeId { get; set; }
        public int? FieldId { get; set; }
        public string Value { get; set; }
    }
}