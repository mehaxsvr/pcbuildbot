using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Interface.Models;

namespace PCBuild.RemoteApi.Fields.Models
{
    public class TypeFieldModel : ApiModel
    {
        public int TypeId { get; set; }
        public int FieldId { get; set; }

        public TypeModel Type { get; set; }
        public FieldModel Field { get; set; }
    }
}