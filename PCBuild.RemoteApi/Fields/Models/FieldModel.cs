using PCBuild.RemoteApi.Interface.Models;

namespace PCBuild.RemoteApi.Fields.Models
{
    public class FieldModel : ApiModel
    {
        public string Name { get; set; }
    }
}