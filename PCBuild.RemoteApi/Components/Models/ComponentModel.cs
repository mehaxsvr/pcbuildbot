﻿using PCBuild.RemoteApi.Interface.Models;

namespace PCBuild.RemoteApi.Components.Models
{
    public class ComponentModel : ApiModel
    {
        public int TypeId { get; set; }
        public string Name { get; set; }

        public TypeModel Type { get; set; }
    }
}
