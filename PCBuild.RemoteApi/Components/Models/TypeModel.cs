using PCBuild.RemoteApi.Interface.Models;

namespace PCBuild.RemoteApi.Components.Models
{
    public class TypeModel : ApiModel
    {
        public string Name { get; set; }
        public string Short { get; set; }
    }
}