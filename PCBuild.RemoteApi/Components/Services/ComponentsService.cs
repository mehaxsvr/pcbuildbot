﻿using PCBuild.RemoteApi.Components.Contracts;
using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Interface.Repositories;
using PCBuild.RemoteApi.Interface.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCBuild.RemoteApi.Components.Services
{
    public class ComponentsService : RemoteApiService<ComponentModel>, IComponentsService
    {
        public ComponentsService(IApiRepository<ComponentModel> repository) : base(repository)
        {
        }
    }
}
