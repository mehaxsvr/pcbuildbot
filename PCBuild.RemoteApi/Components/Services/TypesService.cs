﻿using PCBuild.RemoteApi.Components.Contracts;
using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Interface.Repositories;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuild.RemoteApi.Components.Services
{
    public class TypesService : RemoteApiService<TypeModel>, ITypesService
    {
        public TypesService(IApiRepository<TypeModel> repository) : base(repository)
        {
        }
    }
}
