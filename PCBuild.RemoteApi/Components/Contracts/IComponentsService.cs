﻿using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuild.RemoteApi.Components.Contracts
{
    public interface IComponentsService : IApiService<ComponentModel>
    {
    }
}
