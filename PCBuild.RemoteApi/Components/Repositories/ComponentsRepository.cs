﻿using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Interface.Repositories;

namespace PCBuild.RemoteApi.Components.Repositories
{
    public class ComponentsRepository : RemoteApiRepository<ComponentModel>
    {
        public ComponentsRepository() : base("components",new[] {"type"})
        {
        }
    }
}
