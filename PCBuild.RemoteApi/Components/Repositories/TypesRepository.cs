using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Interface.Repositories;

namespace PCBuild.RemoteApi.Components.Repositories
{
    public class TypesRepository : RemoteApiRepository<TypeModel>, IApiRepository<TypeModel>
    {
        public TypesRepository() : base("types")
        {
        }
    }
}