// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.5.0

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using PCBuildBot.Dialogs;
using PCBuildBot.Dialogs.Components;

namespace PCBuildBot.Bots
{
    public class EchoBot : ActivityHandler
    {
        private readonly BotAccessors _botAccessors;
        private readonly ConversationState _conversationState;
        private readonly UserState _userState;

        public DialogSet Dialogs { get; }

        public EchoBot(BotAccessors botAccessors,
            ConversationState conversationState,
            UserState userState,
            TypesDialog typesDialog,
            DefaultTextPrompt defaultTextPrompt,
            DefaultChoicePrompt defaultChoicePrompt)
        {
            _botAccessors = botAccessors;
            _conversationState = conversationState;
            _userState = userState;

            Dialogs = new DialogSet(_botAccessors.DialogStateAccessor);
            Dialogs.Add(typesDialog);
            Dialogs.Add(defaultTextPrompt);
            Dialogs.Add(defaultChoicePrompt);
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext,
            CancellationToken cancellationToken)
        {
            var activity = turnContext.Activity;

            var dialogContext = await Dialogs.CreateContextAsync(turnContext, cancellationToken);

            var dialogResult = await dialogContext.ContinueDialogAsync(cancellationToken);
            if (!dialogContext.Context.Responded)
            {
                switch (activity.Type)
                {
                    case ActivityTypes.Message:
                        await dialogContext.BeginDialogAsync(TypesDialog.Id, cancellationToken: cancellationToken);
                        break;
                }
            }


            await SaveStateAsync(turnContext);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    var dialogContext = await Dialogs.CreateContextAsync(turnContext, cancellationToken);
                    await turnContext.SendActivityAsync(MessageFactory.Text($"Hello and welcome!"), cancellationToken);
                    await dialogContext.BeginDialogAsync(TypesDialog.Id, cancellationToken: cancellationToken);
                }
            }
        }

        private async Task SaveStateAsync(ITurnContext turnContext)
        {
            await _conversationState.SaveChangesAsync(turnContext);
            await _userState.SaveChangesAsync(turnContext);
        }
    }
}
