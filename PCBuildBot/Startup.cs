﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.5.0

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PCBuild.RemoteApi.Components.Contracts;
using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Components.Repositories;
using PCBuild.RemoteApi.Components.Services;
using PCBuild.RemoteApi.Fields.Contracts;
using PCBuild.RemoteApi.Fields.Models;
using PCBuild.RemoteApi.Fields.Repositories;
using PCBuild.RemoteApi.Fields.Services;
using PCBuild.RemoteApi.Interface.Repositories;
using PCBuildBot.Bots;
using PCBuildBot.Dialogs;
using PCBuildBot.Dialogs.Components;

namespace PCBuildBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add API Repositories
            services.AddSingleton<IApiRepository<TypeModel>, TypesRepository>();
            services.AddSingleton<IApiRepository<ComponentModel>, ComponentsRepository>();
            services.AddSingleton<IApiRepository<FieldModel>, FieldsRepository>();
            services.AddSingleton<IApiRepository<TypeFieldModel>, TypeFieldsRepository>();
            services.AddSingleton<IApiRepository<CaptionModel>, CaptionsRepository>();

            // Add API Services
            services.AddSingleton<ITypesService, TypesService>();
            services.AddSingleton<IComponentsService, ComponentsService>();
            services.AddSingleton<IFieldsService, FieldsService>();
            services.AddSingleton<ICaptionsService, CaptionsService>();

            // Add MVC version
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Inject memory storage
            services.AddSingleton<IStorage, MemoryStorage>();

            // Inject Conversation & User State
            services.AddSingleton<ConversationState>();
            services.AddSingleton<UserState>();

            // Inject Bot accessors
            services.AddSingleton<BotAccessors>();

            // Create the Bot Framework Adapter with error handling enabled.
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            services.AddTransient<IBot, EchoBot>();

            // Inject dialogs
            services.AddTransient<TypesDialog>();
            services.AddTransient<DefaultTextPrompt>();
            services.AddTransient<DefaultChoicePrompt>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
