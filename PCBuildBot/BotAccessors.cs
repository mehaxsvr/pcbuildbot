﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;

namespace PCBuildBot
{
    public class BotAccessors
    {
        public IStatePropertyAccessor<DialogState> DialogStateAccessor { get; }

        public BotAccessors(ConversationState conversationState)
        {
            DialogStateAccessor = conversationState.CreateProperty<DialogState>(nameof(DialogState));
        }
    }
}
