﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using PCBuild.RemoteApi.Components.Models;
using PCBuild.RemoteApi.Components.Contracts;
using PCBuild.RemoteApi.Interface.Services;

namespace PCBuildBot.Dialogs.Components
{
    public class TypesDialog : WaterfallDialog
    {
        private ITypesService _typesService;
        private IComponentsService _componentsService;

        public static new string Id => "typesDialog";
    
        public TypesDialog(ITypesService typesService, IComponentsService componentsService) : base(Id)
        {
            _typesService = typesService;
            _componentsService = componentsService;

            AddStep(InitializeStateStepAsync);
            AddStep(PromptForTypesStepAsync);
            AddStep(PromptForComponentsStepAsync);
            AddStep(EndDialogStepAsync);
        }

        private async Task<DialogTurnResult> InitializeStateStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            return await stepContext.NextAsync(cancellationToken: cancellationToken);
        }

        private async Task<DialogTurnResult> PromptForTypesStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var types = await _typesService.GetAll();

            return await stepContext.PromptAsync(DefaultChoicePrompt.Id, new PromptOptions()
            {
                Choices = types.Select(type => new Choice()
                {
                    Value = type.Name
                }).ToList()
            });
        }

        private async Task<DialogTurnResult> PromptForComponentsStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            var components = await _componentsService.GetAll();

            if (components.Count > 0)
            {
                return await stepContext.PromptAsync(DefaultChoicePrompt.Id, new PromptOptions()
                {
                    Choices = components.Select(component => new Choice()
                    {
                        Value = component.Name
                    }).ToList()
                });
            }

            await stepContext.Context.SendActivityAsync("No components found");
            return await stepContext.NextAsync();
        }

        private async Task<DialogTurnResult> EndDialogStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }
    }
}
