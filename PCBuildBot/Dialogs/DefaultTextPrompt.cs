﻿using Microsoft.Bot.Builder.Dialogs;

namespace PCBuildBot.Dialogs
{
    public class DefaultTextPrompt : TextPrompt
    {
        public static new string Id => "defaultTextPrompt";

        public DefaultTextPrompt() : base(Id)
        {
        }
    }
}
