﻿using Microsoft.Bot.Builder.Dialogs;

namespace PCBuildBot.Dialogs
{
    public class DefaultChoicePrompt : ChoicePrompt
    {
        public static new string Id => "defaultChoicePrompt";

        public DefaultChoicePrompt() : base(Id)
        {
        }
    }
}
